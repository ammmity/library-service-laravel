<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class BooksReserve extends Model
{

    protected $table = 'books_reserve';

    protected $fillable = [
        'book_id', 'user_id', 'issued', 'reserved_from', 'reserved_to'
    ];

    // public function book()
    // {
    //     return $this->belongsTo(Books::class);
    // }
    //
    // public function user()
    // {
    //     return $this->belongsTo(User::class);
    // }
}
