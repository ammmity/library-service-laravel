<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateBook;
use App\User;
use App\Entity\Book;
use App\Entity\BooksReserve;

class BookController extends Controller
{
    public function index(Request $request) {
        $books = Book::with('booksReserve')->get();
        $clients = User::query()->where('role_id', '=', 3)->get();

        //dd($clients->toArray());

        return view('admin.books', [
            'books' => $books,
            'clients' => $clients
        ]);
    }

    public function store(CreateBook $request)
    {
        $book = new Book();
        $book->name = $request->name;
        $book->author = $request->author;
        $book->genre = $request->genre;
        $book->publisher = $request->publisher;
        $saved = $book->save();

        if ($saved) {
            return response()->json($book);
        }

        return response()->json(false);
    }

    public function delete(Request $request)
    {
        $book = Book::with(['booksReserve'])->find($request->id);

        $inReserve = $book->booksReserve()->get()->toArray();

        if (empty($inReserve)) {
            Book::destroy((int) $request->id);
            return response()->json(true);
        }

        return response()->json(['message' => 'Невозможно удалить забронированную книгу'], 422);
    }

    public function issueReservedBook(Request $request)
    {
        $bookReserve = BooksReserve::query()->where([
            ['user_id', $request->user_id],
            ['book_id', $request->book_id]
        ])->first();

        if ($bookReserve) {
            $bookReserve->issued = true;
            $bookReserve->save();
        }

        return response()->json($bookReserve);
    }

    public function takeIssuedBook(Request $request)
    {
        $bookReserve = BooksReserve::query()->where([
            ['user_id', $request->user_id],
            ['book_id', $request->book_id]
        ])->first();

        if ($bookReserve) {
            $bookReserve->delete();
        }

        return response()->json($bookReserve);
    }

}
