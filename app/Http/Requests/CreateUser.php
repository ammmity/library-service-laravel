<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CreateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check() && $this->user()->role_id === 1) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|unique:users|string|email',
            'role_id' => 'required',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Имя - обязательное поле',
            'surname.required' => 'Фамилия - обязательное поле',
            'email.required' => 'Почта - обязательное поле',
            'email.unique' => 'Пользователь с такой почтой уже зарегистрирован',
            'role_id.required' => 'Роль - обязательное поле',
            'password.required' => 'Пароль - обязательное поле',
        ];
    }
}
