@extends('layouts.app')

@section('page_title', 'Архив книг')

@section('content')

<library-component
    v-bind:initial-books="{{ json_encode($books) }}"
></library-component>

@endsection
