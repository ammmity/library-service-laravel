<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();

// Login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Registration
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('/', 'LibraryController@index')->name('library');
Route::post('/library/', 'LibraryController@index')->name('library.json');
Route::post('/library/search/', 'LibraryController@search')->name('library.search');

Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'admin']
    ],
    function () {
        Route::get('/users/', 'UserController@index')->name('admin.users');
        Route::post('/users/change_password/', 'UserController@changePassword')->name('admin.users.change.password');
        Route::post('/users/store/', 'UserController@store')->name('admin.users.store');
        Route::post('/users/delete/', 'UserController@delete')->name('admin.users.delete');
    }
);

Route::group(
    [
        'prefix' => 'library',
        'middleware' => ['auth']
    ],
    function () {
        Route::get('/reserved_books/', 'LibraryController@reservedBooks')->name('library.reserved_books');
        Route::post('/reserve_book/', 'LibraryController@reserveBook')->name('library.reserve_book');
        Route::post('/withdraw_reserve/', 'LibraryController@withdrawReserve')->name('library.withdraw_reserved_books');
    }
);

Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'librarian']
    ],
    function () {
        Route::get('/books/', 'BookController@index')->name('admin.books');
        //Route::post('/books/json/', 'BookController@booksJson')->name('books.json');
        Route::post('/books/store/', 'BookController@store')->name('admin.books.store');
        Route::post('/books/delete/', 'BookController@delete')->name('admin.books.delete');
        Route::post('/users/reserved_books/', 'UserController@reservedBooks')->name('admin.users.reserved.books');
        Route::post('/users/issued_books/', 'UserController@issuedBooks')->name('admin.users.issued.books');
        Route::post('/books/issue_book/', 'BookController@issueReservedBook')->name('admin.books.issue.reserved.book');
        Route::post('/books/take_issued_book/', 'BookController@takeIssuedBook')->name('admin.books.take.issued.book');
        //
    }
);
