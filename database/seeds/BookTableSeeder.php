<?php

use Illuminate\Database\Seeder;
use App\Entity\Book;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');;

        foreach (range(1, 30) as $index) {
            $user = new Book();
            $user->name = $faker->sentence(3);
            $user->author = $faker->name;
            $user->genre = $faker->randomElement([
                'Классика',
                'Комикс',
                'Детектив',
                'Фэнтэзи',
                'История'
            ]);
            $user->publisher = $faker->word;
            $user->save();
        }
    }
}
