<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use App\Entity\BooksReserve;
use App\Http\Requests\CreateUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Entity\Role;
use App\Entity\Book;

class UserController extends Controller
{
    //
    public function index(Request $request) {
        $roles = Role::all();
        $users = User::with('role')->get();

        return view('admin.users', [
            'users' => $users,
            'roles' => $roles,
        ]);
    }

    public function store(CreateUser $request)
    {
        $role = Role::query()->find($request->role_id);

        if (!$role) {
            return response()->json(false);
        }

        $user = new User();
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role()->associate($role);
        $saved = $user->save();

        if ($saved) {
            return response()->json($user);
        }

        return response()->json(false);
    }

    public function delete(Request $request)
    {
        $result = User::destroy((int) $request->id);

        return response()->json($result);
    }

    public function changePassword(Request $request)
    {
        $user = User::query()->findOrFail($request->id);
        $user->password = Hash::make($request->password);
        $result = $user->save();

        return response()->json($result);
    }

    public function reservedBooks(Request $request)
    {
        $reservedBooks = Book::whereHas('booksReserve', function (Builder $q) use ($request) {
            return $q->where([
                ['user_id', '=', $request->user_id],
                ['issued', false]
            ]);
        })->get();

        if ($request->isMethod('post')) {
            return response()->json($reservedBooks);
        }

        return view('userReservedBooks', [
            'reservedBooks' => $reservedBooks
        ]);

    }

     public function issuedBooks(Request $request)
    {
        $reservedBooks = Book::whereHas('booksReserve', function (Builder $q) use ($request) {
            return $q->where([
                ['user_id', '=', $request->user_id],
                ['issued', true]
            ]);
        })->get();
        return response()->json($reservedBooks);
    }

}
