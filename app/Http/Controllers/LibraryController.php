<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Entity\Book;
use App\Entity\BooksReserve;
use Illuminate\Support\Carbon;

class LibraryController extends Controller
{
    public function index(Request $request)
    {
        $books = Book::with('booksReserve')->get();

        if ($request->isMethod('post')) {
            return response()->json($books);
        }

        return view('library', [
            'books' => $books
        ]);
    }

    public function search(Request $request)
    {
        $books = Book::with('booksReserve')
            ->where('name', 'like', $request->search)
            ->orWhere('author', 'like', $request->search)
            ->orWhere('genre', 'like', $request->search)
            ->orWhere('publisher', 'like', $request->search)
            ->get();

        return response()->json($books);
    }

    public function reservedBooks(Request $request)
    {
        $reservedBooks = Book::query()
            ->with('booksReserve')
            ->whereHas('booksReserve', function (Builder $q) use ($request) {
                return $q->where([
                    ['user_id', '=', $request->user()->id],
                    ['issued', false]
                ]);
            })->get();

        return view('libraryReservedBooks', [
            'reservedBooks' => $reservedBooks
        ]);
    }

    public function reserveBook(Request $request)
    {
        $response['message'] = '';
        $book = Book::with(['booksReserve'])->find($request->book_id);

        if ($book->booksReserve === null) {
            $booksReserve = new BooksReserve();
            $booksReserve->user_id = $request->user()->id;
            $booksReserve->issued = false;
            $booksReserve->reserved_from = Carbon::today()->format('Y-m-d');
            $booksReserve->reserved_to = Carbon::tomorrow()->format('Y-m-d');
            $book->booksReserve()->save($booksReserve);
            $response['message'] = 'Книга успешно зарезервированна';
            $response['booksReserve'] = $booksReserve;
        } else {
            $response['message'] = 'Книгу нельзя зарезервировать, т.к она уже зарезервированна другим пользователем';
        }

        return response()->json($response);
    }

    public function withdrawReserve(Request $request)
    {
        $booksReserve = BooksReserve::query()->where([
            ['user_id', $request->user()->id],
            ['book_id', $request->book_id]
        ])->first();

        if ($booksReserve) {
            $response = $booksReserve->delete();
            return response()->json($response);
        }

        return response()->json(false);
    }


}
