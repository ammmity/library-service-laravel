<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        User::query()->create(
            [
                'name' => 'Администратор',
                'surname' => '1',
                'email' => 'admin@test.test',
                'role_id' => 1,
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10)
            ]
        );

        User::query()->create(
            [
                'name' => 'Библиотекарь',
                'surname' => '2',
                'email' => 'librarian@test.test',
                'role_id' => 2,
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10)
            ]
        );

        User::query()->create(
            [
                'name' => 'Клиент',
                'surname' => '3',
                'email' => 'user@test.test',
                'role_id' => 3,
                'password' => Hash::make('password'),
                'remember_token' => Str::random(10)
            ]
        );

        foreach (range(1, 10) as $index) {
            $user = new User();
            $user->name = $faker->firstName;
            $user->surname = $faker->lastName;
            $user->email = $faker->unique()->safeEmail;
            $user->role_id = $faker->randomElement([1, 2, 3]);
            $user->password = Hash::make('password');
            $user->remember_token = Str::random(10);
            $user->save();
        }
    }
}
