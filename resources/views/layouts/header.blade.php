<header class="header fixed-top">
    <div class="header__body">
        <nav class="header__nav navbar navbar-expand-lg navbar-light">

            <div class="header__logo"><a href="{{ route('library') }}">Библиотека</a></div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header__navbar" aria-controls="header__navbar" aria-expanded="false" aria-label="Меню">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="header__navbar collapse navbar-collapse" id="header__navbar">

                @admin
                    <div class="header__navbar-item">
                        <a href="{{ route('admin.users') }}">Управление пользователями</a>
                    </div>
                @endadmin

                @librarian
                    <div class="header__navbar-item">
                        <a href="{{ route('admin.books') }}">Управление книгами</a>
                    </div>
                @endlibrarian

                @user
                    <div class="header__navbar-item">
                        <a class="nav-link" href="{{ route('library.reserved_books') }}">Забронированные книги</a>
                    </div>
                @enduser

                <!-- Authentication Links -->
                @guest
                    <div class="header__navbar-item">
                        <a class="nav-link" href="{{ route('login') }}">Вход</a>
                    </div>
                    @if (Route::has('register'))
                        <div class="header__navbar-item">
                            <a class="nav-link" href="{{ route('register') }}">Регистрация</a>
                        </div>
                    @endif
                @else
                    <div class="header__navbar-item">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                Выход
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                @endguest

            </div>
        </nav>

    </div>
</header>
