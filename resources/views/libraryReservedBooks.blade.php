@extends('layouts.app')

@section('page_title', 'Забронированные книги')

@section('content')

<library-reserved-books-component
    v-bind:initial-books="{{ json_encode($reservedBooks) }}"
></library-reserved-books-component>

@endsection
