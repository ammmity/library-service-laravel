@extends('layouts.app')

@section('page_title', 'Управление книгами')

@section('title')
    <h1>Управление книгами</h1>
@endsection

@section('content')

    <admin-books-component
        v-bind:initial-books="{{ json_encode($books) }}"
        v-bind:clients="{{ json_encode($clients) }}"
    ></admin-books-component>

@endsection
