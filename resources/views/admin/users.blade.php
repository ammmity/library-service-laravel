@extends('layouts.app')

@section('page_title', 'Управление пользователями')

@section('title')
    <h1>Управление пользователями</h1>
@endsection

@section('content')

    <admin-users-component
        v-bind:initial-users="{{ json_encode($users) }}"
        v-bind:roles="{{ json_encode($roles) }}"
    ></admin-users-component>

@endsection
